### 接口内容
```java
package com.fr.design.fun;
import com.fr.design.mainframe.JTemplate;
import java.util.Map;
/**
 * @author richie
 * @date 2015-03-19
 * @since 8.0
 * 自定义预览方式接口
 */
public interface PreviewProvider {

    public static final String MARK_STRING = "PreviewProvider";

    /**
     * 下拉弹出菜单的名字
     * @return 弹出菜单名字
     */
    public String nameForPopupItem();

    /**
     * 下拉弹出菜单的图标路径
     * @return 图标路径
     */
    public String iconPathForPopupItem();

    /**
     * 大图标路径
     * @return 大图标路径
     */
    public String iconPathForLarge();

    /**
     * 点击下拉菜单时触发的事件
     */
    public void onClick(JTemplate<?, ?> jt);

    /**
     * 用于标记预览类型的整数
     * @return 预览类型
     */
    public int previewTypeCode();

    /**
     * 该种预览方式所携带的默认参数集合
     * @return 参数集合
     */
    public Map<String, Object> parametersForPreview();

}
```
_______
### 抽象实现
```java
package com.fr.design.meta;

import com.fr.design.fun.PreviewProvider;
import com.fr.design.mainframe.JTemplate;

import java.util.Collections;
import java.util.Map;

/**
 * @author richie
 * @date 2015-03-19
 * @since 8.0
 */
public abstract class AbstractPreviewProvider implements PreviewProvider {

    @Override
    public void onClick(JTemplate<?, ?> jt) {
        jt.previewMenuActionPerformed(this);
    }

    @Override
    public Map<String, Object> parametersForPreview() {
        return Collections.EMPTY_MAP;
    }
}
```
______
### 分页预览实现
```java
package com.fr.design.preview;

import com.fr.base.io.IOFile;
import com.fr.design.meta.AbstractPreviewProvider;
import com.fr.general.Inter;

/**
 * @author richie
 * @date 2015-03-19
 * @since 8.0
 */
public class PagePreview extends AbstractPreviewProvider {
    @Override
    public String nameForPopupItem() {
        return Inter.getLocText("M-Page_Preview");
    }

    @Override
    public String iconPathForPopupItem() {
        return "com/fr/design/images/buttonicon/pages.png";
    }

    @Override
    public String iconPathForLarge() {
        return "com/fr/design/images/buttonicon/pageb24.png";
    }

    @Override
    public int previewTypeCode() {
        return IOFile.DEFAULT_PREVIEW_TYPE;
    }
}
```
_______
### 填报预览具体实现
```java
package com.fr.design.preview;

import com.fr.base.io.IOFile;
import com.fr.design.meta.AbstractPreviewProvider;
import com.fr.general.Inter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author richie
 * @date 2015-03-19
 * @since 8.0
 */
public class WritePreview extends AbstractPreviewProvider {
    @Override
    public String nameForPopupItem() {
        return Inter.getLocText("M-Write_Preview");
    }

    @Override
    public String iconPathForPopupItem() {
        return "com/fr/design/images/buttonicon/writes.png";
    }

    @Override
    public String iconPathForLarge() {
        return "com/fr/design/images/buttonicon/writeb24.png";
    }

    @Override
    public int previewTypeCode() {
        return IOFile.WRITE_PREVIEW_TYPE;
    }

    @Override
    public Map<String, Object> parametersForPreview() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("op", "write");
        return map;
    }
}
```
___
### 分析预览实现
```java
package com.fr.design.preview;

import com.fr.base.io.IOFile;
import com.fr.design.meta.AbstractPreviewProvider;
import com.fr.general.Inter;

import java.util.HashMap;
import java.util.Map;

/**
 * @author richie
 * @date 2015-03-19
 * @since 8.0
 */
public class ViewPreview extends AbstractPreviewProvider {
    @Override
    public String nameForPopupItem() {
        return Inter.getLocText("M-Data_Analysis");
    }

    @Override
    public String iconPathForPopupItem() {
        return "com/fr/design/images/buttonicon/anas.png";
    }

    @Override
    public String iconPathForLarge() {
        return "com/fr/design/images/buttonicon/anab24.png";
    }

    @Override
    public int previewTypeCode() {
        return IOFile.ANA_PREVIEW_TYPE;
    }

    @Override
    public Map<String, Object> parametersForPreview() {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("op", "view");
        return map;
    }
}
```
