____
### 接口内容
```java
package com.fr.stable.fun;

import com.fr.stable.UrlDriver;

import java.sql.Connection;

/**
 * @since 8.0
 * 数据库方言对象生成接口
 */
public interface DialectCreator {

    public static final String XML_TAG = "DialectCreator";

    /**
     * 根据数据库驱动地址生成数据库方言
     * @param driver 数据库驱动对象
     * @return 数据库方言
     */
    public Class<?> generate(UrlDriver driver);

    /**
     * 根据数据库连接生成数据库方言，如果想使用内置的则返回null就可以
     * @param connection 数据库连接
     * @return 数据库方言
     */
    public Class<?> generate(Connection connection);

}
```
____
### 示例
加入想自定义Sybase数据库的方言，那么按照如下步骤来做：
继承SybaseDialect，重写一个数据库方言的实现

```java
package com.fr.plugin.dialect;

import com.fr.data.core.db.TableProcedure;

import java.sql.Connection;
import java.sql.ResultSet;

public class TestSybaseDialect extends SybaseDialect {
    public TableProcedure[] getTableProcedure(Connection connection, String schema, boolean b) throws Exception {
        return super.getTableProcedure(connection, schema, b);
    }

    public TableProcedure[] getProcedureList(Connection connection, ResultSet result, String catalog, String schema) {
        return super.getProcedureList(connection, result, catalog, schema);
    }
}
```

实现DialectCreator接口

```java
package com.fr.plugin.dialect;

import com.fr.stable.UrlDriver;
import com.fr.stable.fun.DialectCreator;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.SQLException;

/**
 * @author richie
 * @date 15/1/19
 * @since 8.0
 */
public class TestDialectCreator implements DialectCreator {
    public Class<?> generate(UrlDriver driver) {

        return com.fr.plugin.dialect.TestSybaseDialect.class;
    }

    public Class<?> generate(Connection connection) {
        try {
            // 随便测试的代码，根据metaData获取信息
            DatabaseMetaData metaData = connection.getMetaData();
            return null;
        } catch (SQLException e) {
            return com.fr.plugin.dialect.TestSybaseDialect.class;
        }

    }
}
```