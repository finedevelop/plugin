## 核心部分
### 对应的XML标签：extra-core
|接口功能|接口位置|版本|
|----------|----------|------|
|控件样式|[com.fr.stable.fun.FlatWidgetSwitcher](meta/FlatWidgetSwitcher.MD)|8.0|
|附件下载|[com.fr.stable.fun.AttachmentDownloader](meta/AttachmentDownloader.MD)|7.1.1|
|邮件发送配置|[com.fr.stable.fun.EmailProvider](meta/EmailProvider.MD)|7.1.1|
|Excel导出值处理|[com.fr.stable.fun.ExcelExportCellValueProvider](meta/ExcelExportCellValueProvider.MD)|7.1.1|
|自定义单元格打印值|[com.fr.stable.fun.PrintCellValueProvider](meta/PrintCellValueProvider.MD)|7.1.1|
|自定义参数获取规则|[com.fr.stable.fun.RequestParameterHandler](meta/RequestParameterHandler.MD)|7.1.1|
|HTTP请求中获取参数集|[com.fr.stable.fun.RequestParameterCollector](meta/RequestParameterCollector.MD)|7.1.1|
|JS文件引入|[com.fr.stable.fun.JavaScriptFileHandler](meta/JavaScriptFileHandler.MD)|8.0|
|CSS文件引入|[com.fr.stable.fun.CssFileHandler](meta/CssFileHandler.MD)|8.0|
|自定义Web请求|[com.fr.stable.fun.Service](meta/Service.MD)|7.1.1|
|自定义无权限时跳转地址|[com.fr.stable.fun.NoPrivilegeDirector](meta/NoPrivilegeDirector.MD)|7.1.1|
|自定义数据库方言处理|[com.fr.stable.fun.DialectCreator](meta/DialectCreator.MD)|7.1.1|
|HTTP认证返回结果处理|[com.fr.stable.fun.HttpAuthProcessor](meta/HttpAuthProcessor.MD)|7.1.1|

## 报表部分
### 对应的XML标签：extra-report
|接口功能|接口位置|版本|
|----------|----------|-----|
|自定义预览方式|[com.fr.report.fun.ActorProvider](meta/ActorProvider.MD)|7.1.1|

## 设计器部分
### 对应的XML标签：extra-designer
|接口功能|接口位置|版本|
|----------|----------|-----|
|参数界面自定义控件|[com.fr.design.fun.ParameterWidgetOptionProvider](meta/ParameterWidgetOptionProvider.MD)|7.1.1|
|填报自定义控件|[com.fr.design.fun.CellWidgetOptionProvider](meta/CellWidgetOptionProvider.MD)|8.0|
|自定义数据集界面|[com.fr.design.fun.TableDataDefineProvider](meta/TableDataDefineProvider.MD)|7.1.1|
|自定义数据连接|[com.fr.design.fun.ConnectionProvider](meta/ConnectionProvider.MD)|7.1.1|
|自定义预览方式|[com.fr.design.fun.PreviewProvider](meta/PreviewProvider.MD)|8.0|